# Rock-paper-scissors-lizard-Spock template

import random

# The key idea of this program is to equate the strings
# "rock", "paper", "scissors", "lizard", "Spock" to numbers
# as follows:
#
# 0 - rock
# 1 - Spock
# 2 - paper
# 3 - lizard
# 4 - scissors

# helper functions

def number_to_name(number):
    # fill in your code below
    if number == 0:
        return "rock"
    elif number == 1:
        return "Spock"
    elif number == 2:
        return "paper"
    elif number == 3:
        return "lizard"
    elif number == 4:
        return "scissors"
    else:
        print "Error: number_to_name() out of range"
    
    # convert number to a name using if/elif/else
    # don't forget to return the result!

    
def name_to_number(name):
    # fill in your code below
    if name == "rock":
        return 0
    elif name == "Spock":
        return 1
    elif name == "paper":
        return 2
    elif name == "lizard":
        return 3
    elif name == "scissors":
        return 4
    else:
        print "Error: name_to_number() out of range"

    # convert name to number using if/elif/else
    # don't forget to return the result!

def sheldonList(x, y):
    if x == y:
        return number_to_name(x) + " equales " + number_to_name(y)
    elif (x == 4 and y == 2) or (x == 2 and y == 4):
        return "Scissors cuts paper"
    elif (x == 2 and y == 0) or (x == 0 and y == 2):
        return "Paper covers rock"
    elif (x == 0 and y == 3) or (x == 3 and y == 0):
        return "Rock crushes lizard"
    elif (x == 3 and y == 1) or (x == 1 and y == 3):
        return "Lizard poisons Spock"
    elif (x == 1 and y == 4) or (x == 4 and y == 1):
        return "Spock smashes scissors"
    elif (x == 4 and y == 3) or (x == 3 and y == 4):
        return "Scissors decapacitate lizard"
    elif (x == 3 and y == 2) or (x == 2 and y == 3):
        return "Lizard eats paper"
    elif (x == 2 and y == 1) or (x == 1 and y == 2):
        return "Paper disproves Spock"
    elif (x == 0 and y == 4) or (x == 4 and y == 0):
        return "Rock crushes scissors"
    elif (x == 1 and y == 0) or (x == 0 and y == 1):
        return "Spock vaporizez rock"
    else:
        return "Error: sheldonList() out of range"

def rpsls(name): 
    # fill in your code below

    # convert name to player_number using name_to_number
    player_number = name_to_number(name)

    # compute random guess for comp_number using random.randrange()
    # added some pseudo-randomness, don't know if it helps though
    random.seed()
    comp_number = random.randrange(0, 4096) // 1024

    # compute difference of player_number and comp_number modulo five
    diff = (player_number - comp_number) % 5

    # use if/elif/else to determine winner
    if diff == 0:
        #let's say 0 for loose 1 for win 2 for draw
        result = 2
    elif diff < 3:
        result = 1
    elif diff < 5:
        result = 0
    else:
        print "Error: rpsls() out of range"

    # convert comp_number to name using number_to_name
    comp_name = number_to_name(comp_number)
    
    # didn't want to do it like this, but as stated in assignment, i abide
    print "Player chooses: ", number_to_name(player_number)
    print "Computer chooses: ", number_to_name(comp_number)
    print sheldonList(player_number, comp_number)
    
    # print results
    if result == 0:
        print "Computer wins!"
    elif result == 1:
        print "Player wins!"
    elif result == 2:
        print "Player and computer tie!"
    else:
        "Error: result out of range"
    
    print ""
    
# test your code
rpsls("rock")
rpsls("Spock")
rpsls("paper")
rpsls("lizard")
rpsls("scissors")

# always remember to check your completed program against the grading rubric


