# template for "Stopwatch: The Game"
import simplegui

# define global variables
time = 0
points = 0
stops = 0

# define helper function format that converts time
# in tenths of seconds into formatted string A:BC.D
def format(t):
    return str(t//600) + ":" + str((t%600)//100) + str(((t%600)%100)//10) + "." + str(t%10)  
    
# define event handlers for buttons; "Start", "Stop", "Reset"
def start():
    # starts the timer
    timer.start()
    
def stop():
    # stops the timer and counts score
    global points, stops, time
    timer.stop()
    stops += 1
    if time%10 == 0:
        points +=1
    
def reset():
    # stops the timer and resets the game state
    global time, stops, points
    timer.stop()
    time = 0
    stops = 0
    points = 0
    
def show_score():
    # returns score in needed format
    global stops, points
    return str(points) + "/" + str(stops)
    
# define event handler for timer with 0.1 sec interval
def timer_handler():
    global time
    time += 1

# define draw handler
def draw_handler(canvas):
    canvas.draw_text(format(time), [60,110], 40, "White")  
    canvas.draw_text(show_score(), [120, 30], 30, "Green")
    
# create frame
frame = simplegui.create_frame("Stopwatch: The Game", 200, 200)
timer = simplegui.create_timer(100, timer_handler)

# register event handlers
frame.set_draw_handler(draw_handler)
frame.add_button("START", start, 100)
frame.add_button("STOP", stop, 100)
frame.add_button("RESET", reset, 100)

# start frame
frame.start()

# Please remember to review the grading rubric
