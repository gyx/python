# Implementation of classic arcade game Pong

import simplegui
import random

# initialize globals - pos and vel encode vertical info for paddles
WIDTH = 600
HEIGHT = 400       
BALL_RADIUS = 20
PAD_WIDTH = 8
PAD_HEIGHT = 80
HALF_PAD_WIDTH = PAD_WIDTH // 2
HALF_PAD_HEIGHT = PAD_HEIGHT // 2
ball_pos = [WIDTH // 2, HEIGHT // 2]
radius = HEIGHT//20
score1 = 0
score2 = 0
paddle1_pos = 0
paddle2_pos = 0
paddle1_vel = 0
paddle2_vel = 0
ball_vel = [0, 0]
speed = 2
time = 0
tick = 100

# helper function that spawns a ball by updating the 
# ball's position vector and velocity vector
# if right is True, the ball's velocity is upper right, else upper left
def ball_init(right):
    global ball_pos, ball_vel, speed # these are vectors stored as lists
    ball_pos = [WIDTH // 2, HEIGHT // 2]
    ball_vel = [speed * right, speed * right]

# define event handlers

def new_game():
    global paddle1_pos, paddle2_pos, paddle1_vel, paddle2_vel  # these are floats
    global score1, score2, time  # these are ints
    score1 = 0
    score2 = 0
    paddle1_pos = 0
    paddle2_pos = 0
    paddle1_vel = 0
    paddle2_vel = 0
    time = 0
    timer.stop()
    
    if ball_vel[0]>0:
        ball_init(-1)
    else:
        ball_init(1)
        
    timer.start()
    
def input_handler(text_input):
    global speed
    if 0<=int(text_input)<100:
        speed = int(text_input)
        
def timer_handler():
    global time
    time += 1

def draw(c):
    global score1, score2, paddle1_pos, paddle2_pos, ball_pos, ball_vel, radius
 
    # update paddle's vertical position, keep paddle on the screen
    if (paddle1_pos < -(HEIGHT//2)+HALF_PAD_HEIGHT):
        paddle1_pos = -(HEIGHT//2)+HALF_PAD_HEIGHT
    elif (paddle1_pos > (HEIGHT//2)-HALF_PAD_HEIGHT):
        paddle1_pos = (HEIGHT//2)-HALF_PAD_HEIGHT
    else:  paddle1_pos+=paddle1_vel
     
    if (paddle2_pos < -(HEIGHT//2)+HALF_PAD_HEIGHT):
        paddle2_pos = -(HEIGHT//2)+HALF_PAD_HEIGHT
    elif (paddle2_pos > (HEIGHT//2)-HALF_PAD_HEIGHT):
        paddle2_pos = (HEIGHT//2)-HALF_PAD_HEIGHT
    else:  paddle2_pos+=paddle2_vel
    
    # draw mid line and gutters
    c.draw_line([WIDTH / 2, 0],[WIDTH / 2, HEIGHT], 1, "White")
    c.draw_line([PAD_WIDTH, 0],[PAD_WIDTH, HEIGHT], 1, "White")
    c.draw_line([WIDTH - PAD_WIDTH, 0],[WIDTH - PAD_WIDTH, HEIGHT], 1, "White")
    
    # draw paddles
    c.draw_line((HALF_PAD_WIDTH, (HEIGHT//2)-HALF_PAD_HEIGHT+paddle1_pos), (HALF_PAD_WIDTH, (HEIGHT//2)+HALF_PAD_HEIGHT+paddle1_pos), PAD_WIDTH, "White")
    c.draw_line((WIDTH-HALF_PAD_WIDTH,  (HEIGHT//2)-HALF_PAD_HEIGHT+paddle2_pos), (WIDTH-HALF_PAD_WIDTH, (HEIGHT//2)+HALF_PAD_HEIGHT+paddle2_pos), PAD_WIDTH, "White")

    # update ball
    if (ball_pos[1] < 0 + radius):
        ball_vel[1] = -1 * ball_vel[1]
        ball_pos[0]+=ball_vel[0]
        ball_pos[1]+=ball_vel[1]
    elif (ball_pos[1] > HEIGHT - radius):
        ball_vel[1] = -1 * ball_vel[1]
        ball_pos[0]+=ball_vel[0]
        ball_pos[1]+=ball_vel[1]
    elif ball_pos[0] > (WIDTH - radius - PAD_WIDTH):
        ball_vel[0] = -1 * ball_vel[0]
        ball_pos[0]+=ball_vel[0]
        ball_pos[1]+=ball_vel[1]
        score1+=1
    elif ball_pos[0] < (0 + radius + PAD_WIDTH):
        ball_vel[0] = -1 * ball_vel[0]
        ball_pos[0]+=ball_vel[0]
        ball_pos[1]+=ball_vel[1]
        score2+=1
    else:   
        ball_pos[0]+=ball_vel[0]
        ball_pos[1]+=ball_vel[1]
            
    # draw ball and scores
    c.draw_circle(ball_pos, radius, radius, "White", "White")
    c.draw_text(str(score1), (WIDTH//2-WIDTH//4, HEIGHT//5), HEIGHT//4, "White")
    c.draw_text(str(score2), (WIDTH//2+WIDTH//4, HEIGHT//5), HEIGHT//4, "White")
    
def keydown(key):
    global paddle1_vel, paddle2_vel, speed

    if key==simplegui.KEY_MAP["W"]:
        paddle1_vel -= speed
    elif key==simplegui.KEY_MAP["S"]:
        paddle1_vel += speed
    elif key==simplegui.KEY_MAP["down"]:
        paddle2_vel += speed
    elif key==simplegui.KEY_MAP["up"]:
        paddle2_vel -= speed
   
def keyup(key):
    global paddle1_vel, paddle2_vel, speed
    
    if key==simplegui.KEY_MAP["W"]:
        paddle1_vel += speed
    elif key==simplegui.KEY_MAP["S"]:
        paddle1_vel -= speed
    elif key==simplegui.KEY_MAP["down"]:
        paddle2_vel -= speed
    elif key==simplegui.KEY_MAP["up"]:
        paddle2_vel += speed

# create frame
frame = simplegui.create_frame("Pong", WIDTH, HEIGHT)
frame.set_draw_handler(draw)
frame.set_keydown_handler(keydown)
frame.set_keyup_handler(keyup)
frame.add_button("START", new_game, 100)
inp = frame.add_input("Initial speed", input_handler, 50)
timer = simplegui.create_timer(tick, timer_handler)


# start frame
frame.start()
