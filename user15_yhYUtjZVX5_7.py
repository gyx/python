# Mini-project #6 - Blackjack

import simplegui
import random

# load card sprite - 949x392 - source: jfitz.com
CARD_SIZE = (73, 98)
CARD_CENTER = (36.5, 49)
card_images = simplegui.load_image("http://commondatastorage.googleapis.com/codeskulptor-assets/cards.jfitz.png")

CARD_BACK_SIZE = (71, 96)
CARD_BACK_CENTER = (35.5, 48)
card_back = simplegui.load_image("http://commondatastorage.googleapis.com/codeskulptor-assets/card_back.png")    

# initialize some useful global variables
in_play = False
outcome = "Press Deal to start"
score = 0
bet = 0
playerBank = 100
dealerBank = 10000000
dealerValue = ""

# define globals for cards
SUITS = ('C', 'S', 'H', 'D')
RANKS = ('A', '2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K')
VALUES = {'A':1, '2':2, '3':3, '4':4, '5':5, '6':6, '7':7, '8':8, '9':9, 'T':10, 'J':10, 'Q':10, 'K':10}

# define card class
class Card:
    def __init__(self, suit, rank):
        if (suit in SUITS) and (rank in RANKS):
            self.suit = suit
            self.rank = rank
        else:
            self.suit = None
            self.rank = None
            print "Invalid card: ", suit, rank

    def __str__(self):
        return self.suit + self.rank

    def get_suit(self):
        return self.suit

    def get_rank(self):
        return self.rank

    def draw(self, canvas, pos):
        card_loc = (CARD_CENTER[0] + CARD_SIZE[0] * RANKS.index(self.rank), 
                    CARD_CENTER[1] + CARD_SIZE[1] * SUITS.index(self.suit))
        canvas.draw_image(card_images, card_loc, CARD_SIZE, [pos[0] + CARD_CENTER[0], pos[1] + CARD_CENTER[1]], CARD_SIZE)
        
# define hand class
class Hand:
    def __init__(self):
        # create Hand object
        self.hand = []

    def __str__(self):
        # return a string representation of a hand
        result=""
        for x in self.hand:
            result += str(x) + " "
        return result

    def add_card(self, card):
        # add a card object to a hand
        self.hand.append(card)

    def get_value(self):
        # count aces as 1, if the hand has an ace, then add 10 to hand value if it doesn't bust
        # compute the value of the hand, see Blackjack video
        aces = 0
        result = 0
        for x in self.hand:
            result += VALUES[x.rank]
            if x.rank == 'A':
                aces += 1

        if aces == 1 and (result+10 <= 21):
            return result+10
        else: return result
        
    def draw(self, canvas, pos):
        # draw a hand on the canvas, use the draw method for cards
        
        for i in range(len(self.hand)):
            self.hand[i].draw(canvas, (pos[0] + CARD_SIZE[0] * i, pos[1]))
            
                   
# define deck class 
class Deck:
    def __init__(self):
        # create a Deck object
        self.deck = []	
        
        for x in SUITS:
            for y in RANKS:
                 self.deck.append(Card(x, y))

    def shuffle(self):
        # add cards back to deck and shuffle
        # use random.shuffle() to shuffle the deck
        self.__init__()
        random.shuffle(self.deck)

    def deal_card(self):
        # deal a card object from the deck
        return self.deck.pop(0)
    
    def __str__(self):
        # return a string representing the deck
        result=""
        for x in self.deck:
            result += str(x) + " "
        return result

player_hand = Hand()
dealer_hand = Hand()

#define event handlers for buttons

def input_handler(text_input):
    global bet, outcome
    if playerBank >= int(text_input):
        bet = int(text_input)
    else: outcome = "You bet too much"

def deal():
    global outcome, in_play, dealer_hand, player_hand, deck, dealerValue, playerBank, bet, dealerBank, score
    # your code goes here
    dealerValue = ""
    deck = Deck()
    deck.shuffle()
    dealer_hand = Hand()
    player_hand = Hand()
    player_hand.add_card(deck.deal_card())
    dealer_hand.add_card(deck.deal_card())
    player_hand.add_card(deck.deal_card())
    dealer_hand.add_card(deck.deal_card())
    
    if in_play != True:
        print "Player hand: " + str(player_hand) + "Value: " + str(player_hand.get_value())
        print "Dealer hand: " + str(dealer_hand) + "Value: " + str(dealer_hand.get_value())
        outcome = "Hit or Stand?"
    else:
        outcome = "Player surrendered"
        playerBank -= bet
        dealerBank += bet
        score -= 1
        
    in_play = True

def hit():
    # replace with your code below
    global outcome, in_play, dealer_hand, player_hand, deck, playerBank, dealerValue, bet, dealerBank, score
    
    # if the hand is in play, hit the player
    # if busted, assign a message to outcome, update in_play and score
    
    if in_play == True:
        player_hand.add_card(deck.deal_card())
        if player_hand.get_value() > 21:
            print "Busted! Dealer wins"
            outcome = "Busted! Dealer wins"
            playerBank -= bet
            dealerBank += bet
            score -= 1
            in_play = False  
        else: outcome = "Hit or Stand?"
        print "Player hand: " + str(player_hand) + "Value: " + str(player_hand.get_value())            
            
def stand():
    # replace with your code below
    global outcome, in_play, dealer_hand, player_hand, deck, playerBank, bet, dealerBank, score
    # if hand is in play, repeatedly hit dealer until his hand has value 17 or more
    if in_play == True:
        while dealer_hand.get_value() < 17:
            dealer_hand.add_card(deck.deal_card())
            print "Dealer hand: " + str(dealer_hand) + "Value: " + str(dealer_hand.get_value())
        if dealer_hand.get_value() > 21:
            print "Dealer Busted! Gratz! Player wins!"
            outcome = "Dealer Busted! Player Wins!"
            playerBank += bet
            dealerBank -= bet
            score += 1
            in_play = False
        elif dealer_hand.get_value() >= player_hand.get_value():
            print "Dealer wins"
            outcome = "Dealer wins"
            playerBank -= bet
            dealerBank += bet
            score -= 1
            in_play = False
        else:
            print "Gratz! Player wins!"
            outcome = "Player Wins!"
            playerBank += bet
            dealerBank -= bet
            score += 1
            in_play = False
    # assign a message to outcome, update in_play and score
    
# draw handler    
def draw(canvas):
    # test to make sure that card.draw works, replace with your code below
    global outcome, in_play, dealer_hand, player_hand, deck, dealerValue
    
    
    player_hand.draw(canvas, [50, 400])
    dealer_hand.draw(canvas, [50, 200])
    
    if in_play == True:
        canvas.draw_image(card_back, CARD_BACK_CENTER, CARD_BACK_SIZE, [50 + CARD_CENTER[0] , 200 + CARD_CENTER[1]], CARD_BACK_SIZE)
    elif in_play == False:
        dealerValue = str(dealer_hand.get_value())
        
    canvas.draw_text(outcome, (125, 100), 40, "Black")
    canvas.draw_text("BLACKJACK", (125, 50), 60, "Black")
    canvas.draw_text("Player hand: " + str(player_hand.get_value()), (50, 375), 40, "Black")
    canvas.draw_text("Dealer hand: " + dealerValue, (50, 175), 40, "Black")
    canvas.draw_text("Your funds: " + str(playerBank) + " Your Bet: " + str(bet), (25, 525), 30, "Black")
    canvas.draw_text("Your score: " + str(score), (25, 550), 30, "Black")
    canvas.draw_text("Dealer funds: " + str(dealerBank), (25, 325), 30, "Black")
# initialization frame
frame = simplegui.create_frame("Blackjack", 600, 600)
frame.set_canvas_background("Green")

#create buttons and canvas callback
frame.add_button("Deal", deal, 200)
frame.add_button("Hit",  hit, 200)
frame.add_button("Stand", stand, 200)
frame.add_input("Enter bet: ", input_handler, 50)
frame.set_draw_handler(draw)

# get things rolling
frame.start()


# remember to review the gradic rubric