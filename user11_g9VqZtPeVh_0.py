# template for "Guess the number" mini-project
# input will come from buttons and an input field
# all output for the game will be printed in the console

import simplegui
import random
import math

# initialize global variables used in your code

number = 100
guess_num = 1
max_guess_num = 7
game_mode = 0

# define event handlers for control panel
    
def win_check(guess):
    # checking win/lose conditions and restarts game
   global max_guess_num, guess_num, game_mode, number
   if max_guess_num - guess_num == 0 and guess != number:
      print "You lose!"
      if game_mode == 0:
          range100()
      elif game_mode == 1:
          range1000()
      else: print "game_mode error" 
   elif guess == number:
      print "You win!"
      if game_mode == 0:
          range100()
      elif game_mode == 1:
          range1000()
      else: print "game_mode error"

def range100():
    # button that changes range to range [0,100) and restarts
    
    global number, guess_num, max_guess_num, game_mode
    game_mode = 0
    random.seed()
    number = random.randrange(0, 100)
    print "Guess the number in 0-100 range"
    max_guess_num=math.ceil(math.log((100 - 0 + 1), 2))
    guess_num = 1

def range1000():
    # button that changes range to range [0,1000) and restarts
    
    global number, guess_num, max_guess_num, game_mode
    game_mode = 1
    random.seed()
    number = random.randrange(0, 1000)
    print "Guess the number in 0-1000 range"
    max_guess_num=math.ceil(math.log((1000 - 0 + 1), 2))
    guess_num = 1

def get_input(guess):
    # main game logic goes here	
    
    global number, guess_num
    guess = int(guess)
    
    if guess == number:
        print guess, "Try #", guess_num, " - Correct!"
    elif guess < number:
        print guess, "Try #", guess_num, " - Higher"
    elif guess > number:
        print guess, "Try #", guess_num, " - Lower"
    else: print "def_input() error"
        
    print max_guess_num - guess_num, " guesses left"
    
    win_check(guess)
                
    guess_num += 1 
    
# create frame

frame=simplegui.create_frame("Guess the number", 200, 200)

# register event handlers for control elements

frame.add_button("Range 0-100", range100, 100)
frame.add_button("Range 0-1000", range1000, 100)
frame.add_input("Enter your guess: ", get_input, 100)

# start frame

range100()
frame.start()

# always remember to check your completed program against the grading rubric
