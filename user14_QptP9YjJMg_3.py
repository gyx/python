# implementation of card game - Memory

import simplegui
import random

moves = 0
deck = []
exposed = []
state = 0
turn = []
wincheck = 0

# helper function to initialize globals
def init():
    global moves, deck, exposed, state, turn
    turn = [0, 15]
    state = 0
    wincheck = 0
    moves = 0
    deck = range(1,9) + range(1,9)
    random.shuffle(deck)
    exposed = list([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
    label.set_text("Moves = " + str(moves))
    
# define event handlers
def mouseclick(pos):
    # add game state logic here
    global exposed, moves, state, wincheck
        
    if exposed[pos[0] // 50] == 0:
        exposed[pos[0] // 50] = 1  
        
        if wincheck == 8:
           state = 3
    
        if state == 0:
           state = 1
           turn[0] = pos[0] // 50
        elif state == 1:
           state = 2
           turn[1] = pos[0] // 50
           moves += 1
        elif state == 3:
           pass
        elif state == 2:
           if (deck[turn[0]] != deck[turn[1]]) and (turn[0] != turn[1]):
               exposed[turn[0]] = 0
               exposed[turn[1]] = 0
           elif (deck[turn[0]] == deck[turn[1]]) and (turn[0] != turn[1]):
               wincheck += 1
           turn[0] = pos[0] // 50
           state = 1    

       
        
    label.set_text("Moves = " + str(moves))
    
# cards are logically 50x100 pixels in size    
def draw(canvas):
    canvas.draw_line((0, 0), (800, 0), 4, "Red")
    canvas.draw_line((0, 100), (800, 100), 4, "Red")
    canvas.draw_line((800, 0), (800, 100), 4, "Red")
    
    x=10
    for card in deck:
        canvas.draw_text(str(card), (x, 70), 70, "Green")
        canvas.draw_line((x-10, 0), (x-10, 100), 2, "Red")
        x+=50
        
    for x in range(0,16):
        if exposed[x] == 0:
            canvas.draw_polygon([(x*50+1, 50), (x*50+50-1, 50)], 96, "Green")

# create frame and add a button and labels
frame = simplegui.create_frame("Memory", 800, 100)
frame.add_button("Restart", init)
label = frame.add_label("Moves = " + str(moves))

# initialize global variables
init()

# register event handlers
frame.set_mouseclick_handler(mouseclick)
frame.set_draw_handler(draw)

# get things rolling
frame.start()

# Always remember to review the grading rubric